﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PartyInvites.Models
{
    public class GuestResponce
    {
        [Required(ErrorMessage = "Пожалуйста введите имя!")]
        public string Name { get; set; }
        [Required (ErrorMessage = "Введите Email")] 
        [RegularExpression (".+\\@.+\\..+", ErrorMessage = "Введенный Email адресс не корректен!")]
        public string Email { get; set; }
        [Required (ErrorMessage = "Введите номер телефона")]
        public string Phone { get; set; }
        [Required (ErrorMessage = "Укажите пойдете вы, или нет")]
        public bool WillAttend { get; set; }
    }
}
