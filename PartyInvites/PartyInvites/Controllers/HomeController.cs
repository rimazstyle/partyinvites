﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            Greetings();   
            return View("MyView");
        }
        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }
        [HttpPost]
        public ViewResult RsvpForm(GuestResponce guestResponce)
        {
            if (ModelState.IsValid)
            {
                Repository.AddResponce(guestResponce);
                return View("Thanks", guestResponce);
            }
            else
            {
                //Обнаружена ошибка
                return View();
            } 
        }

        public ViewResult ListResponses()
        {
            return View(Repository.Responces.Where(x => x.WillAttend == true));
        }

        public string Greetings()
        {
            int hour = DateTime.Now.Hour;
            if (hour >= 5 && hour <= 11)
            {
                ViewBag.Greeting = "Доброе утро";
            }
            else if (hour >= 12 && hour <= 16)
            {
                ViewBag.Greeting = "Добрый день";
            }
            else if (hour >= 17 && hour <= 21)
            {
                ViewBag.Greeting = "Добрый вечер";
            }
            else
            {
                ViewBag.Greeting = "Доброй ночи";
            }
            return ViewBag.Greeting;
        }
    }
}
